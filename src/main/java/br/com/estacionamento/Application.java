package br.com.estacionamento;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.estacionamento.db.WkDB;

/*
 -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n
 java.io.tmpdir=C:\Users\willian.kirschner\AppData\Roaming\NetBeans\8.1\apache-tomcat-8.0.27.0_base\temp
 */
@SpringBootApplication
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
//        System.setProperty("java.util.logging.SimpleFormatter.format", "%4$s: %1$tb %1$td, %1$tY %1$tH:%1$tM:%1$tS %5$s%6$s%n");
        SpringApplication.run(Application.class, args);
    }

    public Application() {
        System.out.println("Estacionamento started OK!");
        //String propJavaTmp = System.getProperty("java.io.tmpdir");
        String dir = System.getProperty("user.home");
        File fileDB = new File(dir, "estacionamento.db");
        System.out.println("fileDB=" + fileDB.getAbsolutePath());
        WkDB.setFileDB(fileDB);

//        ServerController sc = new ServerController();
//        sc.dbCreate();
    }

}
