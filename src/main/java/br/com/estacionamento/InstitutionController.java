package br.com.estacionamento;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.estacionamento.db.WkDB;
import br.com.estacionamento.resources.Institution;
import br.com.estacionamento.util.Fakes;
import br.com.estacionamento.util.ReplyMessage;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
@RestController
@RequestMapping("/api/institutions")
@CrossOrigin
public class InstitutionController extends AbstractController {

    private final WkDB<Institution> db;

    public InstitutionController() {
        super(InstitutionController.class.getSimpleName());
        db = new WkDB<>(Institution.class);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Institution>> list() {
        List<Institution> selectAll = db.selectAll();
        return new ResponseEntity<>(selectAll, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<ReplyMessage> create(@RequestBody Institution institution) {
        boolean insert = db.insert(institution);
        return returnMsg(insert, "Criou novpo tipo com sucesso!", "Não foi possível criar uma novo tipo!");
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ReplyMessage> edit(@RequestBody Institution institution, @PathVariable(value = "id") int id) {
        institution.setId(id);
        boolean update = db.update(institution);
        return returnMsgUpdate(update);
    }

    @RequestMapping(PATH_FAKES)
    public ResponseEntity<ReplyMessage> createFakes() {
        List<Institution> created = Fakes.createInstitutions();
        created.stream().forEach((element) -> {
            create(element);
        });
        return fakesCreated(created.size());
    }

}
