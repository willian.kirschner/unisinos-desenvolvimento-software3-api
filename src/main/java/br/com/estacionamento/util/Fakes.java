package br.com.estacionamento.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import br.com.estacionamento.resources.Cost;
import br.com.estacionamento.resources.Institution;
import br.com.estacionamento.resources.Note;
import br.com.estacionamento.resources.Ticket;
import br.com.estacionamento.resources.User;
import br.com.estacionamento.resources.UserFull;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
public class Fakes {
    
    public static final SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    
    public static List<Institution> createInstitutions() {
        List<Institution> list = new ArrayList<>();
        
        Institution i1 = new Institution();
        i1.setDescription("Carro");
        list.add(i1);
        
        Institution i2 = new Institution();
        i2.setDescription("Moto");
        list.add(i2);
        
        Institution i3 = new Institution();
        i3.setDescription("Caminhão");
        list.add(i3);
        
        Institution i4 = new Institution();
        i4.setDescription("Onibus");
        list.add(i4);

        return list;
    }    
    
    public static UserFull getUser1() {
        UserFull user1 = new UserFull();
        user1.setName("Controlador do Sistema");
        user1.setEmail("admin@estacionamento.com");
        user1.setPassword("mudar1234");
        user1.setInstitutions(new Integer[]{1});
        user1.setPermissionLevel(0); // 0 - Super Admin (geral)
        user1.setId(-666); // ID específico para ser usado como Token específico
        return user1;
    }    
    
    public static List<UserFull> createUsers() {
        List<UserFull> list = new ArrayList<>();
        
        UserFull user1 = new UserFull();
        user1.setName("João");
        user1.setEmail("willkev@gmail.com");
        user1.setPassword("mudar1111");
        user1.setInstitutions(new Integer[]{1});
        user1.setPermissionLevel(0); // 0 - Super Admin (geral)
        user1.setId(-666); // ID específico para ser usado como Token específico
        list.add(user1);
        
        UserFull user2 = new UserFull();
        user2.setName("Fulano Bento");
        user2.setEmail("user2@email.com");
        user2.setPassword("mudar2222");
        user2.setInstitutions(new Integer[]{1});
        user2.setPermissionLevel(1); // 1 - Admin (por Instituição)
        list.add(user2);
        
        UserFull user3 = new UserFull();
        user3.setName("Dr. Flemming");
        user3.setEmail("user3@email.com");
        user3.setPassword("mudar3333");
        user3.setInstitutions(new Integer[]{1, 2, 3});
        list.add(user3);
        
        UserFull user4 = new UserFull();
        user4.setName("Técnico NET");
        user4.setEmail("user4@email.com");
        user4.setPassword("mudar4444");
        user4.setInstitutions(new Integer[]{1, 2, 3, 4});
        list.add(user4);
        
        UserFull user5 = new UserFull();
        user5.setName("Mario Luigui da Silva");
        user5.setEmail("user5@email.com");
        user5.setPassword("mudar5555");
        user5.setInstitutions(new Integer[]{2});
        list.add(user5);
        
        UserFull user6 = new UserFull();
        user6.setName("Ernesto Silva");
        user6.setEmail("user6@email.com");
        user6.setPassword("mudar6666");
        user6.setInstitutions(new Integer[]{3});
        list.add(user6);
        
        return list;
    }
    
    public static List<Ticket> createTickets(List<User> users) {
        List<Ticket> list = new ArrayList<>();
        
        Ticket t1 = new Ticket();
        t1.setTitle("");
        Date date = new Date();
        t1.setDateOcurrence(dateFormater.format(date));
        date = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 13)); // -13hr
        t1.setPrediction(dateFormater.format(date));
        t1.setDescription("Fonte trifásica queimada por raio no dia 12/07");
        t1.setResponsableId(users.get(0).getId());
        t1.setUserId(users.get(1).getId());
        t1.setSituation("0");
        t1.setState("a");
        t1.setPriority("a");
        list.add(t1);
        
        Ticket t2 = new Ticket();
        t2.setTitle("");
        date = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24)); // -24hr
        t2.setDateOcurrence(dateFormater.format(date));
        date = new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 200)); // +200hr
        t2.setPrediction(dateFormater.format(date));
        t2.setDescription("Quebrou a luz de fundo do aparelho teletransportados de bosons de higges");
        t2.setResponsableId(users.get(2).getId());
        t2.setUserId(users.get(3).getId());
        t2.setSituation("50");
        t2.setState("a");
        t2.setPriority("a");
        list.add(t2);
        
        Ticket t3 = new Ticket();
        t3.setTitle("");
        date = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 10)); // -10hr
        t3.setDateOcurrence(dateFormater.format(date));
        date = new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 63)); // +63hr
        t3.setPrediction(dateFormater.format(date));
        t3.setDescription("");
        t3.setResponsableId(users.get(4).getId());
        t3.setUserId(users.get(5).getId());
        t3.setSituation("0");
        t3.setState("a");
        t3.setPriority("n");
        list.add(t3);
        
        Ticket t4 = new Ticket();
        t4.setTitle("");
        date = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 8)); // -8hr
        t4.setDateOcurrence(dateFormater.format(date));
        date = new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 100)); // +100hr
        t4.setPrediction(dateFormater.format(date));
        t4.setDescription("");
        t4.setResponsableId(users.get(1).getId());
        t4.setUserId(users.get(5).getId());
        t4.setSituation("100");
        t4.setState("f");
        t4.setPriority("b");
        list.add(t4);
        
        Ticket t5 = new Ticket();
        t5.setTitle("");
        date = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 80)); // -80hr
        t5.setDateOcurrence(dateFormater.format(date));
        date = new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 200)); // +200hr
        t5.setPrediction(dateFormater.format(date));
        t5.setDescription("");
        t5.setResponsableId(users.get(2).getId());
        t5.setUserId(users.get(4).getId());
        t5.setSituation("100");
        t5.setState("e");
        t5.setPriority("b");
        list.add(t5);
        
        Ticket t6 = new Ticket();
        t6.setTitle("");
        date = new Date();
        t6.setDateOcurrence(dateFormater.format(date));
        date = new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 19)); // -19hr
        t6.setPrediction(dateFormater.format(date));
        t6.setDescription("");
        t6.setResponsableId(users.get(0).getId());
        t6.setUserId(users.get(3).getId());
        t6.setSituation("50");
        t6.setState("a");
        t6.setPriority("n");
        list.add(t6);        
        
        return list;
    }
    
    public static List<Cost> createCosts(int ticketId, List<User> users) {
        List<Cost> costs = new ArrayList<>();
        // vai pegar randomicamente um user dentro dos disponíveis
        Random random = new Random(System.currentTimeMillis());
        
        Cost c1 = new Cost();
        c1.setValue(120.77);
        c1.setDescription("2 horas");
        c1.setDate(dateFormater.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 111)))); // -111hr
        c1.setTickteId(ticketId);
        c1.setUserId(users.get(random.nextInt(users.size())).getId());
        costs.add(c1);
        
        Cost c2 = new Cost();
        c2.setValue(240.00);
        c2.setDescription("4 horas");
        c2.setDate(dateFormater.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 11)))); // -11hr
        c2.setTickteId(ticketId);
        c2.setUserId(users.get(random.nextInt(users.size())).getId());
        costs.add(c2);
        
        Cost c3 = new Cost();
        c3.setValue(50.00);
        c3.setDescription("50 pila");
        c3.setDate(dateFormater.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 23)))); // -23hr
        c3.setTickteId(ticketId);
        c3.setUserId(users.get(random.nextInt(users.size())).getId());
        costs.add(c3);
        
        return costs;
    }
    
    public static List<Note> createNotes(int ticketId, List<User> users) {
        List<Note> notes = new ArrayList<>();
        // vai pegar randomicamente um user dentro dos disponíveis
        Random random = new Random(System.currentTimeMillis());
        
        Note n1 = new Note();
        n1.setDescription("");
        n1.setDate(dateFormater.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 150)))); // -150hr
        n1.setTickteId(ticketId);
        n1.setUserId(users.get(random.nextInt(users.size())).getId());
        notes.add(n1);
        
        Note n2 = new Note();
        n2.setDescription("");
        n2.setDate(dateFormater.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 300)))); // -300hr
        n2.setTickteId(ticketId);
        n2.setUserId(users.get(random.nextInt(users.size())).getId());
        notes.add(n2);
        
        Note n3 = new Note();
        n3.setDescription("");
        n3.setDate(dateFormater.format(new Date(System.currentTimeMillis() - (1000 * 60 * 60 * 50)))); // -50hr
        n3.setTickteId(ticketId);
        n3.setUserId(users.get(random.nextInt(users.size())).getId());
        notes.add(n3);
        
        return notes;
    }
    
}
